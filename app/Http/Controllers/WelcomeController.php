<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {
        $tarefas = Task::all();

        return view('welcome', compact('tarefas'));
    }
}
