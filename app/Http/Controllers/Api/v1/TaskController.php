<?php

namespace App\Http\Controllers\Api\v1;

use App\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    public function index()
    {
        $tarefas = Task::orderBy('id', 'desc')->get();

        return response()->json($tarefas);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required','min:5','max:100']
        ]);

        $tarefa = Task::create([
            'title' => $request->title
        ]);

        return response()->json($tarefa);
    }

    public function update(Request $request, $id)
    {
        $tarefa = Task::findOrFail($id);

        $request->validate([
            'title' => ['required','min:5','max:100']
        ]);

        $tarefa->update([
            'title' => $request->title
        ]);

        return response()->json($tarefa);
    }

    public function destroy($id)
    {
        $tarefa = Task::findOrFail($id);
        $tarefa->delete();

        return response()->json([
            "error" => false,
            "message" => "Deletado com sucesso"
        ]);
    }
}
